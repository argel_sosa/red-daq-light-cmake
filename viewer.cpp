#include <iostream>
#include <cstring>
#include <vector>
#include <map>

#include <getopt.h>

#include <TFile.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TF1.h>
#include <TApplication.h>
#include <TTimer.h>
#include <TSystem.h>

#include "Decoder.hh"
#include "RDconfig.hh"
#include "EvRaw0.hh"
#include "RDBaseline.hh"
#include "RDFind_min.hh"
#include "RDFiltering.hh"
#include "RDClusterization.hh"

using namespace std;

void viewer(int run, const string& listfile, bool islaser, int sch = -1, const string& customfile="",
            const string& serfile="", const string& mappingfile="", int customnsamples=-1)
{
    auto app = new TApplication("App",nullptr,nullptr);
    int ev = 0;
    std::string datadir;
    auto theDecoder = new Decoder(datadir);
    if (customnsamples>0)
        theDecoder->SetCustomNSamples(customnsamples);

    if (!(theDecoder->OpenFiles(listfile,run)))
    {
        fprintf (stderr,"Unable to open file. Exiting.\n");
        exit(1);
    }

    string configfile =(islaser) ?  "cfg/laser.cfg" :
                                    "cfg/global.cfg";
    if (!customfile.empty())
        configfile = customfile;

    // read configuration file
    RDconfig::GetInstance()->SetIsLaser(islaser);
    auto cfg  = RDconfig::GetInstance()->get_cfg(configfile);
    if (!serfile.empty())
        RDconfig::GetInstance()->SetSERFile(serfile);
    if (!mappingfile.empty())
        RDconfig::GetInstance()->SetConfigFile(mappingfile);
    vector<double> ser = RDconfig::GetInstance()->GetSER();

    for (ulong s = 0; s < ser.size(); s++)
        cout << "Ser " <<  s << " = " << ser[s] << endl;

    double scale = 0.75;
    auto c_top = new TCanvas("c_top", "c_top", 1000*scale, 1000*scale);
    auto c_bottom = new TCanvas("c_bottom", "c_bottom", 1000*scale, 1000*scale);
    auto c_sipm = new TCanvas("viewer_sipm", "viewer_sipm", 1000*scale, 620*scale);
    auto c_scint = new TCanvas("viewer_scint", "viewer_scint", 1000*scale, 1000*scale);

    c_top->Divide(6, 4); // final config
    c_bottom->Divide(2, 2);
    c_scint->Divide(4,4);

    auto timer = new TTimer("gSystem->ProcessEvents();", 50, kFALSE);

    c_top->SetBatch(kFALSE);

    vector<TGraph*> g;
    vector<TGraph*> gf;
    vector<TGraph*> gp;
    vector<TGraph*> gw;
    vector<TGraph*> gc;

    vector<TF1*> fit;

    bool firsttime=true;

    auto theBL = new RDBaseline;
    auto theFM = new RDFind_min();
    Int_t evNumber = 0;
    if (sch >= 0)
        ev = sch;
    //cout << "Sono qui: "<< ev << " " << sch << endl;
    while (ev > -1) {
        // plot waveform with basic reconstruction

        EvRaw0* evRaw = theDecoder->ReadEvent();
        if (theDecoder->IsOver()) //File is over: rewind!
        {
            if (evNumber < ev)
            {
                cout << "The event #" << ev << " does not exist" << endl;
                exit(1);
            }
            theDecoder->CloseFiles(false);
            theDecoder->OpenFiles(listfile,run);
            continue;
        }
        evNumber = evRaw->GetEvHeader()->GetEventNum();
        if (evNumber < ev)
            continue;
        else if (evNumber > ev) //rewind
        {
            delete theDecoder;
            theDecoder = new Decoder(datadir);
            //theDecoder->CloseFiles(false);
            theDecoder->OpenFiles(listfile,run);
            continue;
        }
        cout << "Display event #" << ev << endl;

        //all waveforms here
        map<int, vector<double>*> wfs = evRaw->GetWFs();
        size_t n_samp = evRaw->GetSampleNumber(wfs.begin()->first);
        vector<double> wf_sipm(n_samp,0);

        if (firsttime)
        {
            //if (n_channels <= 16)
            //  c->Divide(4, 4);
            //else
            //  c->Divide(4, 6);
            for (size_t k=0;k<ser.size();k++)
            {
                g.push_back(new TGraph);
                gf.push_back(new TGraph);
                gp.push_back(new TGraph);
                gw.push_back(new TGraph());
                gc.push_back(new TGraph());

                fit.push_back(new TF1(Form("fit%zu", k),
                                      "[0]*x+[1]",
                                      cfg["baseline_start"],
                              cfg["baseline_stop"]));
                fit.at(k)->SetLineWidth(2);
                fit.at(k)->SetLineColor(kRed);
                g.at(k)->SetLineColor(kBlue);
                g.at(k)->SetLineWidth(2);
                gf.at(k)->SetLineColor(kCyan);
                gf.at(k)->SetLineWidth(2);
                gp.at(k)->SetMarkerStyle(23);
                gp.at(k)->SetMarkerColor(kGreen+1);
                gp.at(k)->SetMarkerSize(2);
                gw.at(k)->SetMarkerStyle(22);
                gw.at(k)->SetMarkerColor(kRed);
                gw.at(k)->SetMarkerSize(1.5);
                gc.at(k)->SetMarkerColor(kOrange);
                gc.at(k)->SetMarkerStyle(8);
                gc.at(k)->SetMarkerSize(1.5);
            }
            firsttime = false;
        }

        // single channels
        int ntop(0);
        int nbot(0);
        int nsci(0);

        //Int_t n_channels = evRaw->GetChannelNumber();
        //for (int k = 0; k < n_channels; k++) {
        for (auto &it : wfs)
        {
            int ch = it.first;
            if (ser.at(ch) <= 0)
                continue;
            vector<double>* wf = it.second;

            //c->cd(ch + 1);

            // baseline
            theBL->DoIt(wf,
                        (int) cfg["baseline_start"],
                    (int) cfg["baseline_stop"]);
            double bm = theBL->mean();

            fit.at(ch)->SetParameters(theBL->m(), theBL->q());

            if (RDconfig::GetInstance()->IsSiPM(ch) && ser[ch] > 0)
            {
                for (ulong i = 0; i < n_samp; i++) {
                    wf_sipm[i] += (wf->at(i) - bm)/ser[ch];
                }
            }

            for (ulong i = 0; i < n_samp; i++)
                g.at(ch)->SetPoint(i, i, wf->at(i));
            g.at(ch)->SetTitle(Form("Event %d - Ch %d;samples;ADC", ev, ch));
            // moving average
            auto ff = new RDFiltering(wf, n_samp);
            vector<double>* wa = ff->mavg(cfg["moving_avg"]);
            vector<double>  waa(n_samp);
            vector<double>  wff(n_samp);

            for (ulong a = 0; a < n_samp; a++) {
                waa[a] = wa->at(a);
                wff[a] = wf->at(a);
            }

            delete ff;

            for (ulong i = 0; i < n_samp; i++)
                gf.at(ch)->SetPoint(i, i, waa[i]);

            if (RDconfig::GetInstance()->IsTop(ch)) {
                c_top->cd(1 + ntop);
                g.at(ch)->SetTitle(Form("Ch%d - SiPM Top; Sa; ADC", ch));
                g.at(ch)->Draw("AL");
                gf.at(ch)->Draw("Lsame");

                fit.at(ch)->Draw("same");

                // find minimum
                theFM->DoIt(&wff, (int) cfg["charge_start"], (int) cfg["charge_stop"], (double) bm,
                        (double) cfg["cdf_threshold"], (double) cfg["fixed_thr"]);

                gp.at(ch)->SetPoint(0, theFM->x_min(), theFM->y_min());
                gp.at(ch)->Draw("Psame");
                gc.at(ch)->SetPoint(0, theFM->s_time(), wff[theFM->s_time()]);
                gc.at(ch)->Draw("Psame");

                // integration window
                gw.at(ch)->SetPoint(0, (int) cfg["charge_start"], bm);
                gw.at(ch)->SetPoint(1, (int) cfg["charge_stop"], bm);

                gw.at(ch)->Draw("Psame");
                ntop++;
            }
            if (RDconfig::GetInstance()->IsBottom(ch)) {
                c_bottom->cd(1 + nbot);
                g.at(ch)->SetTitle(Form("Ch%d - SiPM Bottom; Sa; ADC", ch));
                g.at(ch)->Draw("AL");
                gf.at(ch)->Draw("Lsame");

                fit.at(ch)->Draw("same");

                // find minimum
                theFM->DoIt(&wff, (int) cfg["charge_start"], (int) cfg["charge_stop"],
                        (double) bm, (double) cfg["cdf_threshold"], (double) cfg["fixed_thr"]);

                gp.at(ch)->SetPoint(0, theFM->x_min(), theFM->y_min());
                gp.at(ch)->Draw("Psame");
                gc.at(ch)->SetPoint(0, theFM->s_time(), wff[theFM->s_time()]);
                gc.at(ch)->Draw("Psame");

                // integration window
                gw.at(ch)->SetPoint(0, (int) cfg["charge_start"], bm);
                gw.at(ch)->SetPoint(1, (int) cfg["charge_stop"], bm);

                gw.at(ch)->Draw("Psame");
                nbot++;
            }
            if (RDconfig::GetInstance()->IsLSci(ch)) {
                c_scint->cd(1 + nsci);
                g.at(ch)->SetTitle(Form("Ch%d - LSci; Sa; ADC", ch));
                g.at(ch)->Draw("AL");
                gf.at(ch)->Draw("Lsame");

                fit.at(ch)->Draw("same");

                // find minimum
                theFM->DoIt(&wff, (int) cfg["charge_start"], (int) cfg["charge_stop"],
                        (double) bm, (double) cfg["cdf_threshold"], (double) cfg["fixed_thr"]);

                gp.at(ch)->SetPoint(0, theFM->x_min(), theFM->y_min());
                gp.at(ch)->Draw("Psame");
                gc.at(ch)->SetPoint(0, theFM->s_time(), wff[theFM->s_time()]);
                gc.at(ch)->Draw("Psame");

                // integration window
                gw.at(ch)->SetPoint(0, (int) cfg["charge_start"], bm);
                gw.at(ch)->SetPoint(1, (int) cfg["charge_stop"], bm);

                gw.at(ch)->Draw("Psame");
                nsci++;
            }
            if (RDconfig::GetInstance()->IsSi(ch)) {
                c_scint->cd(1 + nsci);
                g.at(ch)->SetTitle(Form("Ch%d - Si; Sa; ADC", ch));
                g.at(ch)->Draw("AL");
                gf.at(ch)->Draw("Lsame");

                fit.at(ch)->Draw("same");

                // find minimum
                theFM->DoIt(&wff, (int) cfg["charge_start"], (int) cfg["charge_stop"],
                        (double) bm, (double) cfg["cdf_threshold"], (double) cfg["fixed_thr"]);

                gp.at(ch)->SetPoint(0, theFM->x_min(), theFM->y_min());
                gp.at(ch)->Draw("Psame");
                gc.at(ch)->SetPoint(0, theFM->s_time(), wff[theFM->s_time()]);
                gc.at(ch)->Draw("Psame");

                // integration window
                gw.at(ch)->SetPoint(0, (int) cfg["charge_start"], bm);
                gw.at(ch)->SetPoint(1, (int) cfg["charge_stop"], bm);

                gw.at(ch)->Draw("Psame");
                nsci++;
            }
        }
        if (ntop > 0) {
            c_top->Modified();
            c_top->Update();
        } else
            delete c_top;

        if (nbot > 0) {
            c_bottom->Modified();
            c_bottom->Update();
        } else
            delete c_bottom;

        if (nsci > 0) {
            c_scint->Modified();
            c_scint->Update();
        } else
            delete c_scint;

        // all sipm sum
        c_sipm->cd();

        auto g_sipm = new TGraph();
        g_sipm->SetTitle(Form("All SiPMs' Sum - Ev. number %d; samples; PE", ev));
        g_sipm->SetLineColor(kBlue);
        g_sipm->SetLineWidth(3);

        for (ulong i = 0; i < n_samp; i++)
            g_sipm->SetPoint(i, i, wf_sipm[i]);
        g_sipm->Draw("AL");

        // moving average
        auto g_sipm_avg = new TGraph();
        g_sipm_avg->SetTitle(Form("All SiPMs' Sum - Ev. number %d; samples; PE", ev));
        g_sipm_avg->SetLineColor(kCyan);
        g_sipm_avg->SetLineWidth(3);

        auto fff = new RDFiltering(&wf_sipm, n_samp);
        vector<double> *wa_sipm = fff->mavg(cfg["moving_avg"]);
        for (ulong i = 0; i < n_samp; i++)
            g_sipm_avg->SetPoint(i, i, wa_sipm->at(i));
        //g_sipm_avg->Draw("Lsame");

        // baseline
        theBL->DoIt(&wf_sipm, cfg["baseline_start"], cfg["baseline_stop"]);
        double bm = theBL->mean();

        // clusterization
        auto cc = new RDClusterization(wa_sipm, (int) cfg["baseline_stop"], n_samp, cfg["cluster_thr"],
                (int) cfg["peak_sigma"], (int) cfg["cluster_start"], (int) cfg["cluster_stop"]);

        vector<int> start_time = cc->start_time();
        vector<int> c_start = cc->start();
        vector<int> c_stop = cc->stop();
        vector<int> c_rep = cc->rep();

        vector<double> min_x, min_y, s_time, f_time;
        int n_cl = start_time.size();

        if (n_cl == 0) {
            cout << "Cluster not found!" << endl;
            start_time.push_back((int) cfg["cluster_start"]);
            c_start.push_back(0);
            c_stop.push_back( (int) cfg["cluster_start"] + (int) cfg["cluster_stop"]);
            min_x.push_back(0);
            min_y.push_back(0);
            n_cl = 1;
        }
        else {
            TGraph *g_cl[n_cl];
            TGraph *gp_cl[n_cl];
            TGraph *gc_cl[n_cl];
            TGraph *gs_cl[n_cl];

            for (int n = 0; n < n_cl; n++) {
                theFM->DoIt(&wf_sipm, c_start[n], c_stop[n], (double) bm, (double) cfg["cdf_threshold"], (double) cfg["fixed_thr"]);
                min_x.push_back(theFM->x_min());
                min_y.push_back(theFM->y_min());
                s_time.push_back(theFM->s_time());
                f_time.push_back(theFM->f_time());

                g_cl[n] = new TGraph();
                g_cl[n]->SetLineColor(kRed); g_cl[n]->SetLineWidth(2);
                g_cl[n]->SetPoint(0, c_start[n], bm);
                g_cl[n]->SetPoint(1, c_start[n], min_y[n]);
                g_cl[n]->SetPoint(2, c_stop[n], min_y[n]);
                g_cl[n]->SetPoint(3, c_stop[n], bm);
                g_cl[n]->Draw("Lsame");

                gp_cl[n] = new TGraph();
                gp_cl[n]->SetMarkerStyle(23);
                gp_cl[n]->SetMarkerColor(kGreen+1);
                gp_cl[n]->SetMarkerSize(2);
                gp_cl[n]->SetPoint(0, min_x[n], min_y[n]);
                gp_cl[n]->Draw("Psame");

                gc_cl[n] = new TGraph();
                gc_cl[n]->SetMarkerStyle(8);
                gc_cl[n]->SetMarkerColor(kOrange);
                gc_cl[n]->SetMarkerSize(2);
                gc_cl[n]->SetPoint(0, s_time[n], wf_sipm[(int) s_time[n]]);
                gc_cl[n]->Draw("Psame");

                gs_cl[n] = new TGraph();
                gs_cl[n]->SetMarkerStyle(33);
                gs_cl[n]->SetMarkerColor(kRed);
                gs_cl[n]->SetMarkerSize(2.5);
                gs_cl[n]->SetPoint(0, f_time[n], 0);
                gs_cl[n]->Draw("Psame");
            }
        }
        delete cc;
        // cumulative sum
        /*
        auto g_cum = new TGraph();
        g_cum->SetLineColor(kViolet); g_cum->SetLineWidth(2);

        vector<double> *cs = fff->cumsum();

        if (min_y[0] != 0) {
            for (ulong i = 0; i < n_samp; i++) {
                cs->at(i) = cs->at(i)/cs->at(n_samp - 1)*min_y[0];
                g_cum->SetPoint(i, i, cs->at(i));
            }
        }

        g_cum->Draw("same");
        */
        // draw canvas
        c_sipm->Modified();
        c_sipm->Update();

        delete fff;

        timer->TurnOn(); // for intective modification of the canvas
        timer->Reset();

        // progress opt
        char cev[256];
        int rev;
        cout << "Insert event number, RETURN to continue or -1 to quit: ";
        cin.getline(cev, 256);
        rev = atoi(cev);
        if (rev < 0) {
            cout << "Goodbye!" << endl;
            ev = rev;
            // return;
        }
        else if (rev == 0) ev++;
        else ev = rev;

        timer->TurnOff();
    }

    //Cleanup
    for (size_t i=0; i<g.size(); i++)
    {
        delete g.at(i);
        delete gf.at(i);
        delete gp.at(i);
        delete gw.at(i);
        delete fit.at(i);
    }
    delete theBL;
    delete theFM;
    //clean exit
    //theDecoder->CloseFiles(false);
    delete theDecoder;
    delete app;
}

int main(int argc, char *argv[])
{
    int c;
    std::string listfile;
    int nevent = -1;
    int runnr = 0;
    bool islaser = false;
    std::string configfile;
    std::string serfile;
    std::string mappingfile;
    int customnsamples = -1;
    static struct option long_options[] =
    {
        {"run",required_argument,nullptr,'r'},
        {"list",required_argument,nullptr,'l'},
        {"nevent",required_argument,nullptr,'n'},
        {"config",required_argument,nullptr,'c'},
        {"mapping",required_argument,nullptr,'m'},
        {"ser",required_argument,nullptr,'e'},
        {"laser",no_argument,nullptr,'s'},
        {"nsamples",required_argument,nullptr,'a'},
        {"help",no_argument,nullptr,'h'},
        {nullptr, 0, nullptr, 0}
    };

    // getopt_long stores the option index here.
    int option_index = 0;

    // Parse options
    while ((c = getopt_long(argc, argv, "r:l:n:c:e:m:a:sh", long_options, &option_index)) != -1)
    {
        switch (c)
        {
        case 'r':
            if (runnr!=0) {
                fprintf (stderr, "Error while processing option '-r'. Multiple runs specified.\n");
                exit(1);
            }
            if ( sscanf(optarg,"%d",&runnr) != 1 ) {
                fprintf (stderr, "Error while processing option '-r'. Wrong parameter '%s'.\n", optarg);
                exit(1);
            }
            if (runnr<0) {
                fprintf (stderr, "Error while processing option '-r'. Run number set to %d (must be >=0).\n", runnr);
                exit(1);
            }
            fprintf(stdout,"Merging files from run %d\n",runnr);
            break;
        case 'n':
            if ( sscanf(optarg,"%d",&nevent) != 1 ) {
                fprintf (stderr, "Error while processing option '-n'. Wrong parameter '%s'.\n", optarg);
                exit(1);
            }
            //nevent = atoi(optarg);
            fprintf(stdout,"Starting from event %d\n",nevent);
            break;
        case 'l':
            listfile = optarg;
            fprintf(stdout,"Data will be read from files listed in '%s'\n",listfile.c_str());
            break;
        case 'c':
            configfile = optarg;
            fprintf(stdout,"Using custom config file: %s \n",configfile.c_str());
            break;
        case 'e':
            serfile = optarg;
            fprintf(stdout,"Using custom ser file: %s \n",serfile.c_str());
            break;
        case 's':
            islaser = true;
            fprintf(stdout,"This is a laser runs");
            break;
        case 'a':
            customnsamples = atoi(optarg);
            if (customnsamples<=0)
            {
                fprintf (stderr, "Error while processing option '--nsamples'. Wrong parameter '%s'.\n", optarg);
                exit(1);
            }
            fprintf(stdout,"Set custom number of samples %d\n",customnsamples);
            break;
        case 'm':
            mappingfile = optarg;
            fprintf(stdout,"Using channel mapping from file: %s \n",mappingfile.c_str());
            break;
        case 'h':
            fprintf(stdout,"\nviewer ([-r run_number]|[-l list_file]) [-n eventNb] [-c config file]  [--laser] [-h]\n\n");
            fprintf(stdout,"  -r: define run to process\n");
            fprintf(stdout,"  -l: define file with list of data files to process\n");
            fprintf(stdout,"      n.b. either -r or -l must be specified\n");
            fprintf(stdout,"  -n: event number to be displayed (default: %d)\n",nevent);
            fprintf(stdout,"  -m: define channel mapping from file (default: cfg/channelmapping.cfg)\n");
            fprintf(stdout,"  -c: define custom config file\n");
            fprintf(stdout,"  --ser (or -e): Read SERs from a custom file (default: cfg/ser.cfg)\n");
            fprintf(stdout,"  --laser (or -s): laser run (default = normal run)\n");
            fprintf(stdout,"  --nsamples (or -a): override the number of samples from the event header (use with care!) \n");
            fprintf(stdout,"  -h: show this help message and exit\n\n");
            exit(0);
        case '?':
            fprintf (stderr,"Unknown option character `\\x%x'.\n",optopt);
            exit(1);
        default:
            abort();
        }
    }

    // Verify that some input was specified
    if ( listfile.empty() && runnr==0 ) {
        fprintf (stderr,"No run number and no list file were specified. Exiting.");
        exit(1);
    }
    if ( !listfile.empty() && runnr!=0 ) {
        fprintf (stderr,"Both run number and list file were specified. Exiting.");
        exit(1);
    }

    //Main call
    viewer(runnr,listfile,islaser,nevent,configfile,serfile,mappingfile,customnsamples);

    return 0;
}
