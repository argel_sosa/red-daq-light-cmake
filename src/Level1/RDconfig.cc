#include "RDconfig.hh"
#include <iostream>
#include <fstream>
#include <map>

RDconfig* RDconfig::fInstance = 0;

//=============================================================================
RDconfig::RDconfig() : 
  fIsSERread(false),fIsLaser(false),fIsConfigRead(false)
{
  //Defaults
  fSERFile = "cfg/ser.cfg";
  fConfigFile = "cfg/channelmapping.cfg";

  
  TString A[6][4] = {{"A1", "A2", "A3", "A4"},
		     {"B1", "B2", "A5", "B4"},
		     {"C1", "C2", "B3", "B5"},
		     {"C5", "D1", "C4", "C3"},
		     {"D5", "E2", "D4", "D3"},
		     {"E3", "E4", "D2", "E5"}};
  for (int iy = 0; iy < 6; iy++) {
    for (int ix = 0; ix < 4; ix++) {  
      std::pair<int,int> pos(ix,iy);
      fCoordinates.insert(std::pair<TString,std::pair<int,int> >
			  (A[iy][ix],pos));
    }
  }
}
//=============================================================================

void RDconfig::SetConfigFile(TString val)
{
  fConfigFile = val;
}
//=============================================================================
void RDconfig::InitializeChannelMapping()
{
  
 ifstream mapfile(fConfigFile.Data());

  //No file: use default values
  if (!mapfile.is_open()) 
    {
      cout << endl;
      fprintf (stderr,"No channel map file %s was found. Exit now!",
	       fConfigFile.Data());
      std::exit(1);
    } 
  else //Read from the file 
    {
      cout << endl;
      cout << "CHANNEL MAP from file " << fConfigFile << ": " << endl;
         
      for (int i=0; mapfile.good(); i++) 
	{ 
	  int val;
	  TString key;
	  mapfile >> val >> key;
	  // Avoids double-reading of last line
	  if (!mapfile.eof()) 
	    {  
	      fMap.insert ( std::pair<int,TString>(val,key) );      
	      fInvMap.insert ( std::pair<TString,int>(key,val) );   
	      cout << "Channel: " << val << "-->" << key << endl;
	    }
	}   
      mapfile.close();
      if (fMap.size() > 0)
	cout << "Read " << fMap.size() << " channels from file " << endl;
      else
	{
	  cerr << "No chans could be read from the file " <<  fConfigFile  
	       << endl;
	  cerr << " Please check" << endl;
	  std::exit(1);
	}  
    }
  fIsConfigRead = true;
}


//=============================================================================
void RDconfig::InitializeSER()
{
  //If it is a laser run, reset all SERs to 1
  if (fIsLaser) 
    {
      for (int i=0;i<48;i++)
	fSer.push_back(1.);
      cout << "Laser run -> SER == 1 for all channels! " << endl;
      cout << "  all other SER settings are ignored " << endl;
      fIsSERread = true;
      return;
    }

  ifstream serfile(fSERFile.c_str());

  //No file: use default values
  if (!serfile.is_open()) 
    {
      cout << endl;
      fprintf (stderr,"No SER file %s was found. Reading the hard-coded ones..",
	       fSERFile.c_str());
      cout << endl;
      
      double tempser[32];
      for (int tt = 0; tt < 32; tt++) tempser[tt] = 2500;  // FIXME put some reasonable values
      for (int i=0;i<32;i++)
	fSer.push_back(tempser[i]);
    }
  else //Read from the file 
    {
      cout << endl;
      cout << "SER CONFIGURATION from file " << fSERFile << ": " << endl;
         
      for (int i=0; serfile.good(); i++) 
	{ 
	  double val;
	  serfile >> val;
	  // Avoids double-reading of last line
	  if (!serfile.eof()) 
	    {	      
	      fSer.push_back(val);
	      //cout << val << endl;
	    }
	}   
      serfile.close();
      if (fSer.size() > 0)
	cout << "Read " << fSer.size() << " SER values from file " << endl;
      else
	{
	  cerr << "No SER could be read from the file " <<  fSERFile  << endl;
	  cerr << " Please check" << endl;
	  std::exit(1);
	}  
    }
  fIsSERread = true;
  return;
}  


//=============================================================================

RDconfig* RDconfig::GetInstance()
{
  if (!fInstance)
    fInstance = new RDconfig();
  return fInstance;
}

//=============================================================================

std::map<string, double> RDconfig::get_cfg(string fname) 
{
  char cfg_name[100]; 
  double cfg_val;
  map<string, double> cfg;
  
  //First, check that file exists
  ifstream testfile(fname.c_str());
  if (!testfile.is_open())
    {
      cerr << "The file " << fname << " does not exist!" << endl;
      abort();
    }
  testfile.close();
  
  //Ok, now read the TTree
  fCfg_tree = new TTree("cfg_tree", "Configuration file tree");
  fCfg_tree->ReadFile(fname.c_str(), "cfg_name/C:cfg_val/D");
  
  fCfg_tree->SetBranchAddress("cfg_name", &cfg_name);
  fCfg_tree->SetBranchAddress("cfg_val", &cfg_val);
  
  cout << endl << "RECONSTRUCTION CONFIGURATION from file " << fname << ": " << endl;
  for (int i = 0; i < fCfg_tree->GetEntries(); i++) {
      fCfg_tree->GetEntry(i);
      cout << cfg_name << "\t" << cfg_val << endl;
      string s_cfg_name = cfg_name;
      cfg[s_cfg_name] = cfg_val;
  } 
  
  return cfg;
}

//=============================================================================
bool RDconfig::IsTop(int chanID) 
{
  return (GetChannelType(chanID) == kSiPMTop);
}

//=============================================================================
bool RDconfig::IsBottom(int chanID) 
{
  return (GetChannelType(chanID) == kSiPMBottom);
}

//=============================================================================
bool RDconfig::IsSiPM(int chanID) 
{
  return ((GetChannelType(chanID) == kSiPMTop) || 
	  (GetChannelType(chanID) == kSiPMBottom));
}

//=============================================================================
bool RDconfig::IsLSci(int chanID) 
{
  return (GetChannelType(chanID) == kLSci);
}

//=============================================================================
bool RDconfig::IsSi(int chanID) 
{
  return (GetChannelType(chanID) == kSi);
}

//=============================================================================
TTree* RDconfig::GetConfigurationTree()
{
  return fCfg_tree;
}

//=============================================================================
std::vector<double> RDconfig::GetSER()
{
  if (!fIsSERread)
    InitializeSER();
  return fSer;
}

//=============================================================================
std::vector<double> RDconfig::GetLSciCalib()
{
  // hard coded LSCi calibration FIXME
  // this calib is based on runs 566-580, see Run-DB Campaign VI (excel)
  double calib[9] = {7520/59.5, 15130/59.5, 6972/59.5, 6510/59.5, 5995/59.5, 6716/59.5, 7361/59.5, 6005/59.5, 6722/59.5}; 
  for (int i = 0; i < 9; i++ ) fLSciCalib.push_back(calib[i]);

  return fLSciCalib;
}

TString RDconfig::GetChannelName(int chanID)
{
  if (!fIsConfigRead)
    InitializeChannelMapping();
  if (!fMap.count(chanID))
    return "null";
  return fMap.find(chanID)->second;
}

RDconfig::ChannelType RDconfig::GetChannelType(int chanID)
{
  if (!fIsConfigRead)
    InitializeChannelMapping();
  if (!fMap.count(chanID))
    return kUndefined;
  TString val = fMap.find(chanID)->second;
  if (val.BeginsWith("A") || val.BeginsWith("B") || 
      val.BeginsWith("C") || val.BeginsWith("D") || 
      val.BeginsWith("E"))
    {
      return (val.Contains("OFF")) ?  kSiPMTopOFF : kSiPMTop;
    }
  else if (val.BeginsWith("F"))
    {
      return (val.Contains("OFF")) ?  kSiPMBottomOFF : kSiPMBottom;
    }
  else if (val.BeginsWith("L"))
    return kLSci;
  else if (val.BeginsWith("S"))
    return kSi;
  else
    return kUndefined;
  /*
  if (fConfigName == "PF")
    {
      if (chanID == 9 || chanID == 11 || chanID == 13 || chanID == 15)
	return kSiPMBottom; 
      else if (chanID < 28)
	return kSiPMTop;
      else
	return kUndefined;
    }
  else if (fConfigName == "nGun")
    {
      if (chanID==15)
	return kLSci;
      else
	return kUndefined;
    }
  else
    {
      cout << "Invalid configuration name " << fConfigName << endl;
      return kUndefined;
    }
  */
}

//=============================================================================
/*
int RDconfig::FindChannelID(int raw,int column)
{
  TString A[6][4] = {{"A1", "A2", "A3", "A4"},
		     {"B1", "B2", "A5", "B4"},
		     {"C1", "C2", "B3", "B5"},
		     {"C5", "D1", "C4", "C3"},
		     {"D5", "E2", "D4", "D3"},
		     {"E3", "E4", "D2", "E5"}};

  if (raw>3 || column>6)
    return -1;
  TString name = A[column][raw];
  if (!fInvMap.count(name))
    return -1;
  //Find channel type
  int channumber = fInvMap.find(name)->second;
  return channumber;
}
*/

//=============================================================================
std::pair<int,int> RDconfig::FindCoordinates(int channumber)
{
  std::pair<int,int> result(-1,-1);

  if (!IsTop(channumber))
    return result;
  if (!fMap.count(channumber))
    return result;
  TString channame = fMap.find(channumber)->second;
  if (!fCoordinates.count(channame))
    return result;
  /*cout << "det: " << channumber << " --" << channame << " " << 
    fCoordinates.find(channame)->second.first << " " << 
    fCoordinates.find(channame)->second.second << endl;
  */
  return fCoordinates.find(channame)->second;
}
