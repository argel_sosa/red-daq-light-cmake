#include "RDPulseFitter.hh"

//============================================================

RDPulseFitter::RDPulseFitter()
{;}

//============================================================

RDPulseFitter::~RDPulseFitter()
{;}

//============================================================

void RDPulseFitter::FitSPE()
{ ; }

//============================================================

void RDPulseFitter::FitS2()
{ ; }

//============================================================

int RDPulseFitter::GetnFits()
{
    return nFits;
}

//============================================================

int RDPulseFitter::GetFitStatus(const int ifit)
{
    return status.at(ifit);
}

//ClassImp (RDPulseFitter)
