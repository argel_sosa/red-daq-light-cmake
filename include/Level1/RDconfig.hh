#ifndef _RDCONFIG_H 
#define _RDCONFIG_H 

#include <iostream>
#include <cstring>
#include <utility>
#include <vector>
#include <map>
#include <string>

#include <TString.h>
#include <TTree.h>

using namespace std;

class RDconfig
{
 public:
  static RDconfig* GetInstance();
  std::map<string, double> get_cfg(string fname); 
  size_t SERsize(){return fSer.size();};
  size_t LScisize(){return fLSciCalib.size();};

  std::vector<double> GetSER();
  std::vector<double> GetLSciCalib();

  std::string GetSERFile(){return fSERFile;};
  void SetSERFile(std::string aname){fSERFile=std::move(aname);};
  bool IsLaser(){return fIsLaser;};
  void SetIsLaser(bool val){fIsLaser = val;};
  void SetConfigFile(TString val);

  enum ChannelType {kSiPMTop=1, kSiPMBottom=2, kLSci=3, kSi=4, 
		    kUndefined=5, kSiPMTopOFF=6, kSiPMBottomOFF=7};
  ChannelType GetChannelType(int chan);  
  TString GetChannelName(int chanID);

  TTree* GetConfigurationTree();
  //int FindChannelID(int row,int column);
  std::pair<int,int> FindCoordinates(int channumber);

  bool IsTop(int chan);
  bool IsBottom(int chan);
  bool IsSiPM(int chan);
  bool IsLSci(int chan);
  bool IsSi(int chan);


 private:
  RDconfig(); 
  static RDconfig* fInstance;
  std::vector<double> fSer;
  std::vector<double> fLSciCalib;
  std::string fSERFile;
  std::map<int,TString> fMap; //key=channel number
  std::map<TString,int> fInvMap; //key=detector name
  std::map<TString, std::pair<int,int> > fCoordinates;
  TString fConfigFile;
  bool fIsSERread;

  bool fIsLaser;

  void InitializeSER();
  void InitializeChannelMapping();
  bool fIsConfigRead;

  TTree* fCfg_tree;
};

#endif
