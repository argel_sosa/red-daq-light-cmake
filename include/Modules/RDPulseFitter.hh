#ifndef _RDPulseFitter_
#define _RDPulseFitter_

#include "TObject.h"

#include <vector>

using namespace std;

class RDPulseFit;

class RDPulseFitter : public TObject
{ 
public:
    RDPulseFitter();
    ~RDPulseFitter() override;
    void FitSPE();
    void FitS2();
    int GetnFits();
    int GetFitStatus(int ifit);

private:
    //data members
    vector<RDPulseFit*> fits;

    int nFits;
    vector<int> status, covstatus, ndf;
    vector<double> chi2;
    vector<double> par, epar;
    int start, end;
    int sipm;      // [-1] All [isipm]
    int type;      // [0] SPE fit [1] S2 fit

    //ClassDef(RDPulseFitter, 11);
};

#endif
